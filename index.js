// task 1
function vowelCount(str) {
  var vowels = ["a", "e", "i", "o", "u"];
  return str.split("").reduce((acc, curr) => {
    if (vowels.includes(curr)) {
      return acc + 1;
    }
    return acc;
  }, 0);
}

// task 2
function daysTillNewYear() {
  var now = new Date().getTime();
  var newYear = new Date(`${new Date().getFullYear() + 1}/01/01`).getTime();
  var diff = Math.round((newYear - now) / 1000 / 3600 / 24);
  return diff;
}

// task 3
function findSymbol(arr) {
  return arr.filter((el) => {
    return (
      (`${el}`.charCodeAt() < 48 || `${el}`.charCodeAt() > 57) &&
      (`${el}`.charCodeAt() < 65 || `${el}`.charCodeAt() > 90) &&
      (`${el}`.charCodeAt() < 97 || `${el}`.charCodeAt() > 122)
    );
  });
}

// task 4
function intersection(arr1, arr2) {
  return arr1.reduce((acc, curr) => (arr2.includes(curr) ? curr : acc));
}

// task 5
function notify(msg) {
  setTimeout(() => alert(msg), 4000);
}

// task 6
function sumNatural(arr) {
  return arr.reduce((acc, curr) => {
    if (typeof curr == "number" && curr > 0 && curr % 1 == 0) {
      return acc + curr;
    }
    return acc;
  }, 0);
}

// task 7
function charCount(str, char) {
  let counter = 0;
  str.split("").forEach((el) => {
    if (el == char) counter++;
  });
  return counter;
}
